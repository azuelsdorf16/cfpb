import pandas as pd
from operator import itemgetter
from collections import OrderedDict
from multiprocessing import Pool
import numpy as np

def main():
    #cfpb_data = pd.read_csv("first1000.csv").fillna("")
    cfpb_data = pd.read_csv("Consumer_Complaints.csv").fillna("")

    relevant_data = cfpb_data[["Company", "Product", "Consumer disputed?"]]

    #relevant_data = relevant_data[relevant_data["Product"].str.contains(pat=".*payday.*", case=False)]

    relevant_data["Company Product Complaints"] = relevant_data.groupby(["Company", "Product"])["Product"].transform('count')

    relevant_data["Company Product Complaints Resolved"] = relevant_data.groupby(["Company", "Product", "Consumer disputed?"])["Consumer disputed?"].transform(lambda column : len([_ for _ in column if _ != "Yes"]))

    relevant_data["Percent Company Product Complaints Resolved"] = 100. * relevant_data["Company Product Complaints Resolved"] / relevant_data["Company Product Complaints"]

    relevant_data["Company Complaints"] = relevant_data.groupby(["Company"])["Company"].transform('count')

    relevant_data["Company Complaints Resolved"] = relevant_data.groupby(["Company", "Consumer disputed?"])["Consumer disputed?"].transform(lambda column : len([_ for _ in column if _ != "Yes"]))

    relevant_data["Percent Company Complaints Resolved"] = 100. * relevant_data["Company Complaints Resolved"] / relevant_data["Company Complaints"]

    relevant_data.drop_duplicates(inplace=True)

    relevant_data.sort_values(by=["Percent Company Complaints Resolved", "Percent Company Product Complaints Resolved"], ascending=False, inplace=True)

    ordered_columns = ["Company", "Company Complaints Resolved", "Company Complaints", "Percent Company Complaints Resolved", "Product", "Company Product Complaints Resolved", "Company Product Complaints", "Percent Company Product Complaints Resolved"]

    relevant_data.to_csv("output.csv", columns=ordered_columns, index=False)

if __name__ == "__main__":
    main()
