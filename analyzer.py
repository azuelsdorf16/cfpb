import pandas as pd
import matplotlib.pyplot as plot
import numpy as np
import operator
import collections
import sys

def main():
    data = pd.read_csv('Consumer_Complaints.csv', delimiter=",").fillna("")
    #data = pd.read_csv('first1000.csv', delimiter=",").fillna("")

    #Get collection of company names, excluding redundant names.
    complaint_companies = set(data["Company"])

    #Determine ratio of resolved complaints to total complaints per company.
    company_info = dict()

    for company in complaint_companies:
        company_info[company] = dict()

        #get indices for complaints that deal with this company
        company_indices = data["Company"] == company
        
        #get indices for this company's resolved complaints.
        resolved_indices = data["Consumer disputed?"] == "No"
        
        #get indices for this company's student loans (if any).
        student_loan_indices = data["Product"] == "Student loan"

        #get this company's total number of complaints
        #(The number of rows for this company)
        company_info[company]["total"] = len(data[company_indices & student_loan_indices])

        #Get the number of resolved complaints for this company (number of
        #rows that mention this company and have a "Consumer disputed?"
        #value equal to "No")
        company_info[company]["resolved"] = len(data[company_indices & resolved_indices & student_loan_indices])

        if company_info[company]["total"] > 0:
            company_info[company]["ratio"] = float(company_info[company]["resolved"]) / company_info[company]["total"]
        else:
            company_info[company]["ratio"] = "undefined"

    #Sort companies from largest number of complaints to smallest number
    #of complaints.
    #sorted_complaints = collections.OrderedDict(sorted(resolved_complaints.items(),
#key=operator.itemgetter(1), reverse=True))

    for company, complaints in company_info.items():
        if complaints["ratio"] != "undefined":
            print("Resolved complaints for \"{0}\" : {1} out of {2} ({3}%)".format(company, complaints["resolved"], complaints["total"], 100 * complaints["ratio"]))

if __name__ == "__main__":
    main()
